import axios from "axios";

const api = axios.create({
  //baseURL: "http://localhost:8000/",
  baseURL: "https://api-portafolio-2svz.onrender.com/",
});

export const setAuthorizationToken = (token) => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export default api;

//export const domainName = "http://localhost:8000/";
export const domainName = "https://api-portafolio-2svz.onrender.com/";
