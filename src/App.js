import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import PortfolioUI from "./pages/PortfolioUI";
import Login from "./components/User/Login/Login";
import SideBar from "./components/Admin/SideBar/SideBar";
import EducationAdmin from "./pages/EducationAdmin";
import { ToastContainer } from "react-toastify";
import history from "./shared/history";
import SecureRoute from "./shared/SecureRoute";
import ExperienceAdmin from "./pages/ExperienceAdmin";
import SkillAdmin from "./pages/SkillAdmin";
import MessageAdmin from "./pages/MessageAdmin";
import isLogin from "./shared/authorization";
import ProjectAdmin from "./pages/ProjectAdmin";
import NotFound from "./pages/NotFound";

function App() {
  const [isLogged, setIsLogged] = useState(isLogin);
  const login = useSelector((state) => state.login.isLogin);

  useEffect(() => {
    setIsLogged(isLogin);
  }, [login]);

  return (
    <div className="App">
      <BrowserRouter navigator={history}>
        {isLogged && <SideBar />}
        <Routes>
          <Route path="/login" element={<Login />} />

          <Route
            path="/education"
            element={<SecureRoute component={EducationAdmin} />}
          />
          <Route
            path="/experience"
            element={<SecureRoute component={ExperienceAdmin} />}
          />
          <Route
            path="/project"
            element={<SecureRoute component={ProjectAdmin} />}
          />
          <Route
            path="/skill"
            element={<SecureRoute component={SkillAdmin} />}
          />
          <Route
            path="/messages"
            element={<SecureRoute component={MessageAdmin} />}
          />

          <Route path="/" element={<PortfolioUI />} />

          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
      <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default App;
