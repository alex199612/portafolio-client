/*import React, { useState } from "react";
import { Navigate, Route } from "react-router-dom";
import isLogin from "./authorization";

const SecureRoute = (props) => {
  const [isLogged] = useState(isLogin);
  return (
    <Route
      path={props.path}
      render={(data) =>
        isLogged ? (
          <props.component {...data}></props.component>
        ) : (
          <Navigate
            to={{ pathname: "/", state: { from: props.location } }}
          ></Navigate>
        )
      }
    ></Route>
  );
};

export default SecureRoute;
*/

import React, { useState } from "react";
import isLogin from "./authorization";
import { Navigate } from "react-router-dom";

const ProtectedRoutes = ({ component: Component, ...rest }) => {
  const [isLogged] = useState(isLogin);

  console.log(isLogged);
  return isLogged ? <Component /> : <Navigate to="/" />;
};

export default ProtectedRoutes;
