import React from "react";
import profil from "../../../assets/images/profil.png";
import "./About.css";

const About = ({ reff }) => {
  return (
    <section
      ref={reff}
      id="about"
      className="py-5"
      style={{ backgroundColor: "#fff" }}
    >
      <div className="container">
        <h2 className="h1-responsive font-weight-bold text-center mb-5">
          About Me
        </h2>

        <div className="row">
          <div className="col-12 col-lg-4">
            <img
              className="d-block mx-auto mb-4"
              src={profil}
              alt="profil"
              width="250"
              height="250"
            />
          </div>
          <div className="col-lg-8">
            <p className="text-font text-justify">
              Me considero una persona apasionada por la tecnologia.
              Constantemente estoy capacitándome para estar al día en las nuevas
              tendencias tecnológicas. Motivado y con los conocimientos
              adquiridos me desempeño en el campo laboral, mostrando siempre
              capacidad, responsabilidad, seriedad y puntualidad en el
              cumplimiento de mis labores.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
